<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/cast', 'CastController@index'); // menampilkan halaman form
Route::get('/cast/create', 'CastController@create'); // menampilkan halaman form
Route::post('/cast', 'CastController@store'); // menyimpan data
Route::get('/cast/{id}', 'CastController@show'); // menampilkan detail item dengan id 
Route::get('/cast/{id}/edit', 'CastController@edit'); // menampilkan form untuk edit item
Route::put('/cast/{id}', 'CastController@update'); // menyimpan perubahan dari form edit
Route::delete('/cast/{id}', 'CastController@destroy'); // menghapus data dengan id