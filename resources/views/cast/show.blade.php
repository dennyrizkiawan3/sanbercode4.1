@extends('layouts/master')
@section('content')
<div class="m-2">
    <h5>Nama: {{$cast->nama}}</h5>
    <div class="card">
        <div class="card-header">
            <a href="{{url('cast')}}" class="btn btn-sm btn-primary float-right"> Kembali </a>
        </div>
        <div class="card-body">
            <p>
            Umur: {{$cast->umur}}<br>               
            </p>
        </div>
        <div class="card-footer">
            Bio: {{$cast->bio}}
        </div>
    </div>    
</div>
@endsection