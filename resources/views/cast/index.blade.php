@extends('layouts/master')
@section('content')
<div class="row">
    <h3>All Casts</h3>
</div>
<div class="row">
	<a href="{{url('cast/create')}}" class="btn btn-primary float float-right">Buat Cast</a>
</div>
<div class="row mt-2">
<!-- <div class="col"> -->
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Cast</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col" style="display: inline">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cast as $c)
            
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$c->nama}}</td>
                                
                <td>{{$c->umur}}</td>
                <td>{{$c->bio}}</td>
                <td class="text-center">
                	<a href="{{url('/cast/'.$c->id)}}" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i></a>
                    <a href="{{url('/cast/'.$c->id.'/edit')}}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                    <form action="/cast/{{$c->id}}" method="post" style="display: inline;">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
<!-- </div> -->
</div>
@endsection
