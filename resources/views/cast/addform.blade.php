<?php
date_default_timezone_set("Asia/Jakarta");
$now = new DateTime();
?>
@extends('layouts.master')
@section('content')
<br><br>
<div class="row col-7">
    <h3>Create Cast</h3>
</div>
<div class="row card">
    <div class="card-body">
        <form action="{{url('/cast/')}}" method="post">
            @csrf
            <input type="hidden" name="created_at" value="<?php echo $now->format('Y-m-d H:i:s'); ?>">
            <input type="hidden" name="updated_at" value="<?php echo $now->format('Y-m-d H:i:s'); ?>">
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" id="nama" placeholder="Nama Cast..." name="nama">
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" id="umur" placeholder="Umur Cast..." name="umur">
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Bio..." name="bio"></textarea>
            </div>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
</div>
@endsection