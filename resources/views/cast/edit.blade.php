<?php
date_default_timezone_set("Asia/Jakarta");
$now = new DateTime();
?>
@extends('layouts/master')
@section('content')
<div class="row m-2">
    <h3>Edit Cast</h3>
</div>
<div class="card m-2">
    <div class="card-body">
        <form action="{{url('/cast/'.$cast->id)}}" method="POST">
            @csrf
            @method('put')
            <input type="hidden" name="date_created" value="{{$cast->created_at}}">
            <input type="hidden" name="updated_at" value="<?php echo $now->format('Y-m-d H:i:s'); ?>">
            <div class="form-group">
                <label for="formGroupExampleInput">Nama Cast</label>
                <input type="text" class="form-control" id="nama" placeholder="Nama cast" name="nama" value="{{$cast->nama}}">
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" id="umur" placeholder="Umur" name="umur" value="{{$cast->umur}}">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Bio</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Bio..." name="bio">{{$cast->bio}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary float-right">Update</button>
        </form>
    </div>
</div>
@endsection