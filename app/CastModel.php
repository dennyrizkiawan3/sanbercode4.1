<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CastModel
{
    public static function getAll()
    {
        $cast = DB::table('cast')->get();
        return $cast;
    }

    public static function getById($id)
    {
        $cast = DB::table('cast')->where('id', '=', $id)->first();
        return $cast;
    }

    public static function save($cast)
    {
        $new_cast = DB::table('cast')->insert($cast);
        return 1;
    }

    public static function update($id, $request)
    {
        
        $cast = DB::table('cast')->where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
            'updated_at' => $request['updated_at']
        ]);
        return $cast;
    }

    public static function destroy($id)
    {
        $deleted = DB::table('cast')->where('id', $id)->delete();
        return $deleted;
    }
}
